Assessment: Step-by-Step Creation of a Website
To review what you've learned so far, today you will follow a tutorial that guides you step-by-step through the process of looking at a design mockup of a web page, writing HTML for all the elements on the page, and then progressively adding style rules until the page looks like the mockup.Please create new folder in you local workspace called '08-create-website', open this folder in VSCode and start creating.

Practical Exercise: Step by Step Creation of a Website

For today, you can skip the section titled "Ensuring Compatibility with IE".

Most of the techniques used in this exercise should look familiar at this point, but at times you may also find it helpful to refer to earlier chapters from that course, for example Borders and Shadowing.

When you've got the page appearing as expected, upload '08-create-website' folder from your local workspace to your Cloud9 environment, and submit your Cloud9 env URL.

Optional
If you have extra time, we encourage you to try applying some of these styling techniques to enhance the appearance of your own bio page from last week.